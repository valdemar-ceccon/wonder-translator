package translator

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/translate"
)

type Translator struct {
	profileName string
	client      translate.Client
}

type TranslatorConfig func(*Translator)

func WithProfileName(name string) TranslatorConfig {
	return func(t *Translator) {
		t.profileName = name
	}
}

func NewTranslator(translatorConfigs ...TranslatorConfig) (*Translator, error) {
	ret := &Translator{}

	for _, translatorConfig := range translatorConfigs {
		translatorConfig(ret)
	}

	cfg, err := config.LoadDefaultConfig(context.Background(), config.WithSharedConfigProfile(ret.profileName))

	if err != nil {
		return nil, fmt.Errorf("can't create translator: NewTranslator: %s", err)
	}

	ret.client = *translate.NewFromConfig(cfg)

	return ret, nil
}

func shuffle(arr []Lang) []Lang {
	ret := make([]Lang, len(arr))
	copy(ret, arr)
	source := rand.NewSource(time.Now().Unix())
	r := rand.New(source)
	for i := 0; i < len(ret); i++ {
		idx := r.Intn(len(ret))
		ret[idx], ret[i] = ret[i], ret[idx]
	}

	return ret
}

var ErrLangNotFound error = errors.New("lang not found: %s")

func GetLangByCode(code string) (Lang, error) {

	for _, lang := range GetSupportedLangs() {
		if lang.GetCode() == code {
			return lang, nil
		}
	}

	return Lang{}, fmt.Errorf("lang not found: %s", code)
}

func AllBut(lang Lang) []Lang {
	foundidx := -1

	for i, l := range GetSupportedLangs() {
		if l.GetCode() == lang.GetCode() {
			foundidx = i
			break
		}
	}

	return append(GetSupportedLangs()[:foundidx], GetSupportedLangs()[foundidx+1:]...)
}

func CreateRandomLangsSequence(first Lang, amount int) []Lang {
	selectedLangs := AllBut(first)

	if len(selectedLangs) < amount {
		selectedLangs = shuffle(selectedLangs)
	} else {
		selectedLangs = shuffle(selectedLangs)[0:amount]
	}
	selectedLangs = append(selectedLangs[:0], append([]Lang{first}, selectedLangs[0:]...)...)
	selectedLangs = append(selectedLangs, first)

	return selectedLangs
}

func (t *Translator) SequencedTranslation(txt string, langs []Lang) (string, error) {
	current := langs[0]
	langsButFirst := langs[1:]
	curText := txt
	for _, lang := range langsButFirst {
		var err error
		curText, err = t.translateText(current.GetCode(), lang.GetCode(), curText)
		current = lang

		if err != nil {
			return "", fmt.Errorf("can't translate: SequencedTranslation: %s", err)
		}
	}

	return curText, nil
}

func (t *Translator) translateText(src, dst, txt string) (string, error) {
	translateInput := &translate.TranslateTextInput{
		SourceLanguageCode: &src,
		TargetLanguageCode: &dst,
		Text:               &txt,
	}

	translatedText, err := t.client.TranslateText(context.Background(), translateInput)

	if err != nil {
		return "", fmt.Errorf("can't translate: translateText: %s", err)
	}

	return *translatedText.TranslatedText, nil
}
