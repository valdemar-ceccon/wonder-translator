FROM node AS front

WORKDIR /code

COPY frontend/ /code

RUN ls -la

RUN yarn install && yarn build

FROM golang AS back

WORKDIR /code
COPY server/ /code/server/
COPY translator/ /code/translator/
COPY go.mod .
COPY go.sum .

RUN go build -o server-bin server/main.go 

FROM golang

WORKDIR /

COPY --from=back /code/server-bin /
COPY --from=front /code/public/ /frontend/public

RUN ls -la 
RUN ls -la /frontend
RUN touch server.log

CMD ["/server-bin"]
