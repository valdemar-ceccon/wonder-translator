package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/valdemar-ceccon/wonder-translator/translator"
)

type Lang struct {
	Code string
	Name string
}

func main() {
	text := "Never gonna give you up, Never gonna let you down."
	firstLangCode := "en"
	var args []string
	if len(os.Args) > 1 {
		args = os.Args[1:]
		firstLangCode = args[0]
		args = args[1:]
	}

	if len(args) > 1 {
		text = strings.Join(args, " ")
	}
	fmt.Println(os.Args[1:])

	firstLang, err := translator.GetLangByCode(firstLangCode)

	must(err)

	t, err := translator.NewTranslator(translator.WithProfileName("translate"))

	must(err)
	langs := translator.CreateRandomLangsSequence(firstLang, 10)
	translated, err := t.SequencedTranslation(text, langs)

	must(err)

	fmt.Println(translated)
}

func must(err error, custom ...string) {
	if err != nil {
		if len(custom) > 0 {
			strings.Join(custom, "\n")
		}
		panic(err)
	}
}
