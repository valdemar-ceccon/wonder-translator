import json
import sys

def main(inp: str):
    if not inp.endswith(".txt") and not inp.endswith(".tvs"):
        return None
    with open(inp, 'r') as raw_text:
        lines = raw_text.readlines()    
    
    if inp.endswith(".txt"):
        clean_lines = [l.strip() for l in lines if l.strip()]
        pairs = []
        while clean_lines:
            pairs.append(tuple(clean_lines[0:2]))
            clean_lines = clean_lines[2:]
        return {a:b for a,b in pairs}
    if inp.endswith("tvs"):
        clean_lines = [{"code": l.split("\t")[1].strip(), "name": l.split("\t")[0].strip()} for l in lines if l.strip()]
        return clean_lines
        
    

if __name__ == "__main__":
    args = sys.argv[1:]

    out = "output.json"
    inp = None
    if args:
        inp = args[0]
        args = args[1:]
    else:
        print("input mandatory")
        sys.exit(1)
    if args:
        output = args[0]
    ret = main(inp)
    if not ret:
        print(f"file not supported: {inp}")
        sys.exit(1)

    with open(output, 'w') as json_file:
        json_file.write(json.dumps(ret))