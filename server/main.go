package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/valdemar-ceccon/wonder-translator/translator"
)

type TranslateRequest struct {
	Text     []string
	LangCode string
	Amount   int
}

type TranslateResponse struct {
	Text  []string
	Langs []translator.Lang
}

type TranslatorHandler struct {
	t *translator.Translator
}

func (th *TranslatorHandler) Translate(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	log.Println(req)

	if req.Body != nil {
		var data TranslateRequest
		err := json.NewDecoder(req.Body).Decode(&data)

		if serverError(resp, err) {
			return
		}
		log.Printf("%s - %d - %s", data.LangCode, data.Amount, data.Text)
		translated, err := th.execTranslation(&data)

		if serverError(resp, err) {
			return
		}

		json.NewEncoder(resp).Encode(translated)
		log.Printf("langs: %v. result: %v", translated.Langs, translated.Text)
	}
}

func serverError(resp http.ResponseWriter, err error) bool {
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		resp.Write([]byte("Error 500. Call someone."))
		log.Printf("Error: %s", err.Error())
		return true
	}

	return false
}

func (th *TranslatorHandler) execTranslation(treq *TranslateRequest) (*TranslateResponse, error) {
	ret := TranslateResponse{}
	ret.Text = make([]string, len(treq.Text))
	lang, err := translator.GetLangByCode(treq.LangCode)

	if err != nil {
		return &TranslateResponse{}, err
	}
	langs := translator.CreateRandomLangsSequence(lang, treq.Amount)
	ret.Langs = langs
	for i, v := range treq.Text {
		t, err := th.t.SequencedTranslation(v, langs)
		if err != nil {
			return &TranslateResponse{}, err
		}
		ret.Text[i] = t
	}

	return &ret, nil
}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func GetSupportedLangs(resp http.ResponseWriter, req *http.Request) {
	fmt.Println("chamou")
	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	json.NewEncoder(resp).Encode(translator.GetSupportedLangs())
}

func ApiDefaultOptions(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	if req.Method == http.MethodOptions {
		resp.Write([]byte("OK"))
	}
}

func main() {
	log.Println("This is a test log entry")

	t, err := translator.NewTranslator()

	must(err)

	translatorHandler := TranslatorHandler{
		t: t,
	}

	log.Println("Listening on :8000...")

	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()
	r.Handle("/", http.FileServer(http.Dir("./frontend/public"))).Methods(http.MethodGet)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./frontend/public"))))

	r.HandleFunc("/", translatorHandler.Translate).Methods(http.MethodPost)
	r.HandleFunc("/", ApiDefaultOptions).Methods(http.MethodOptions)

	r.HandleFunc("/langs", GetSupportedLangs)
	// r.HandleFunc("langs", ApiDefaultOptions).Methods(http.MethodOptions)

	srv := http.Server{
		Handler: r,
		Addr:    ":8000",
	}

	log.Fatal(srv.ListenAndServe())
}
